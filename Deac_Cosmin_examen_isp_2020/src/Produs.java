import javax.swing.*;

public class Produs extends JFrame {

    JTextField t1;
    JTextField t2;
    JTextField t3;
    JButton button;

    Produs() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(null);
        setSize(150, 250);
        init();
        setVisible(true);
    }

    public void init() {
        t1 = new JTextField();
        t1.setBounds(10, 10, 100, 30);

        t2 = new JTextField();
        t2.setBounds(10, 50, 100, 30);

        t3 = new JTextField();
        t3.setBounds(10, 90, 100, 30);
        t3.setEditable(false);
        add(t1);
        add(t2);
        add(t3);

        button=new JButton("Calculeaza");
        button.setBounds(10,130,100,30);
        button.addActionListener(e->{
            double a=Double.parseDouble(t1.getText());
            double b=Double.parseDouble(t2.getText());

            double c=a*b;
            t3.setText(c+"");
        });
        add(button);

    }

    public static void main(String[] args) {
        new Produs();
    }
}
